export type AbstractNewable = abstract new(...args: any) => any;
