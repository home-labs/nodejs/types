import { Newable } from "./newable.mjs";


export type TypeFromAFieldname<T extends (Newable | Object), N extends string> =
    T extends {
        [P in N]: infer I
    }
        ? I
        : never
;


/*
class C1 {

    property = C1;

    static staticProperty = new C1;

}

let instanceSide: TypeFromAFieldname<C1, 'property'>; //=> C1
let staticSide: TypeFromAFieldname<typeof C1, 'staticProperty'>; //=> typeof C1
/**/
