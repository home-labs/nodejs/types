export type ReturnTypeFromAnyFunctionType<T extends Function | ((...args: any) => any)> =
    T extends {
        (...args: any): infer I
    }
        ? I
        : never
;


// type test = ReturnTypeFromAnyFunctionType<() => string> //=> string
