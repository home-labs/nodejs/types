export type TuplePush<T extends any[], V> = [...T, V];

export type TupleUnshift<T extends any[], V> = [V, ...T];

export type TupleShift<
    T extends any[]
> =
    T extends [infer F, ...infer R]
        ? R
        : never
;

export type FlattenList<
    T extends any[],
    Q extends any[] = [],
    R extends any[] = []
> =
    T['length'] extends 0
        ? Q['length'] extends 0
            ? R
            : FlattenList<Q[0], TupleShift<Q>, R>
        : T[0] extends any[]
            ? FlattenList<TupleShift<T>, TuplePush<Q, T[0]>, R>
            : FlattenList<TupleShift<T>, Q, TuplePush<R, T[0]>>


/*
let pushTest: TuplePush<[1, 2], 3>

let unshiftFest: TupleUnshift<[2, 3], 1>

let shiftTest: TupleShift<[1, 2, 3]>

let flattenTest: FlattenList<[1, [2, 3], [[4], 5]]>
/**/
