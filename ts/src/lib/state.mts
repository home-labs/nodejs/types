type FieldsMap<T> = {
    [N in keyof T]: T[N] extends Function ? never : N
}

export type State<T, E extends string = ''> = {
    [N in FieldsMap<T>[Exclude<keyof T, E>]]: T[N]
};


/*
class C1 {

    f1 = 1;
    f2 = 2;

    static sf1 = 1;

    static sf2 = 2;

    m1() {

    }

    static sm1() {

    }

}

type test = FieldsMap<C1>
type staticTest = FieldsMap<typeof C1>

// map all fields, except
let test: State<C1, 'f2'>
test.f1 //=> ok
test.f2 //=> oops

let staticTest: State<typeof C1, 'sf2'>
staticTest.sf1 //=> ok
staticTest.sf2 //=> oops
/**/
