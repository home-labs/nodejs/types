import { Newable } from "./index.mjs";
import { UnionSet2IntersectionSet } from "./unionSets.mjs";
import { TupleOfInstancesSide } from "./tupleOfInstancesSide.mjs";


/**
 *
 * [number] cria um conjunto de elementos não repetíveis, indexan o tipo do elemento, não o
 *  elemento em si.
 */
export type IntersectionSetAsNewable<TT extends (Newable | Object)[]> = {
    [P in keyof TT]: {
        new(...args: any): UnionSet2IntersectionSet<TupleOfInstancesSide<TT>[number]>
    }
}[number];


/*
class C1 {

    private p1 = 1;

}
class C2 {

    private p2 = 2;

}
class C3 {

    private p3 = 3;

}

//=> new (...args: any[]) => C1 & C2 & C3
let noi1: IntersectionSetAsNewable<[typeof C1, typeof C2, typeof C3]>;
let noi11: IntersectionSetAsNewable<[C1, C2, C3]>;
/**/
