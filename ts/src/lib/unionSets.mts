import { TupleUnshift } from './tuples.mjs';


export type UnionSet2IntersectionSet<T> =
    (T extends any
        ? {
            (arg4CompareSignature: T): void
        }
        : never
    ) extends {
        (arg4CompareSignature: infer I): void
    }
        ? I
        : never
;


/**
 *
 * A | B accepts A or B or A & B type, because that is called UNION of types, a & b only accepts a & b.
 * Para estes dois funcionarem, as classes não podem ter propriedades privadas de mesmo nome rsrs :'(
 */
// type staticMembersIntersectionByType = UnionSet2IntersectionSet<typeof C1 | typeof C2 | typeof C3>; //=> typeof C1 & typeof C2 & typeof C3
// type instanceMembersIntersectionByType = UnionSet2IntersectionSet<C1 | C2 | C3>; //=> C1 & C2 & C3


/**
 *
 * essa é uma forma de captar um dos elementos de todo o conjunto, geralmente este elemento é o
 * que aparece na extrema direta, contudo, por se tratar de um conjunto, não há como garantir a posição
 * dos elementos, já que eles não são enumerados, o que significa que a utilização com tuplas é fortemente desaconselhado.
 */
type LastDeclaredElementOfUnionSet<U, T = void> =
    // o resultado será { anAnyMethodSignature(): T¹ } & { anAnyMethodSignature(): T² } & { anAnyMethodSignature(): T³ } & ...
    UnionSet2IntersectionSet<
        U extends any
            // o resultado será { anAnyMethodSignature(): T¹ } | { anAnyMethodSignature(): T² } | { anAnyMethodSignature(): T³ } | ...
            ? {
                anAnyMethodSignature(): U
            }
            : never
    /**
     *
     * Funciona porque interseções (&) tratam os objetos como de uma mesma classe, em primeiro lugar, em segundo o JS não suporta "overloading", apenas
     * "overriding", isto é, uma classe pode ter vários métodos com o mesmo nome, contudo, porque o JS é fracamente tipado, não haverá análise dos
     * argumentos destes métodos, de forma que o último declarado sobrescreverá o penúltimo, que sobrescreve, caso exista, o antepenúltimo, e assim
     * sucessivamente, o que significa que apenas o último será levado em conta, logo, o TS o tratará da mesma forma.
     *
     * See more:
     * https://www.typescriptlang.org/docs/handbook/2/conditional-types.html#inferring-within-conditional-types
     * "[...]
     * When inferring from a type with multiple call signatures (such as the type of an overloaded function), inferences are made from the last
     * signature[...]".
     */
    > extends {
        anAnyMethodSignature(): infer I
    }
        ? [T] extends [void]
            ? I
            : I extends T
                ? I
                : LastDeclaredElementOfUnionSet<Exclude<U, I>, T>
        : never


/*
type test1 = LastDeclaredElementOfUnionSet<'a' | 'b' | 'c'>
type test2 = LastDeclaredElementOfUnionSet<3 | '4' | 5, string>

type test3 = LastDeclaredElementOfUnionSet<false | true, boolean>

// um tipo abstrato sobrepõe um tipo literal...
type test4 = LastDeclaredElementOfUnionSet<string | 'a'>
type test5 = LastDeclaredElementOfUnionSet<number | 4>
// ... às vezes :'(
type test6 = LastDeclaredElementOfUnionSet<[] | Array<any>>
/**/

/**
 *
 * aqui já é o demônio agindo. O TS, sei lá por que diabos, guarda os números em cache, e neste caso, pega, da esquerda para a direita, o que ainda não
 * foi usado.
 */
// type test7 = LastDeclaredElementOfUnionSet<4 | 2 | 1 | 3, number>


export type UnionSet2RandomList<
    T,
    R extends any[] = [],
    L = LastDeclaredElementOfUnionSet<T>
> =
    [T] extends [never]
        ? R
        : UnionSet2RandomList<Exclude<T, L>, TupleUnshift<R, L>>
;


/*
type abc2 = 1 | 'b' | true;
type abc3 = never;
/**/

/*
type abc = 'a' | 'b' | 'c';

let exclude1: Exclude<abc, 'b'>; //=> 'a' | 'c'
let exclude2: Exclude<'a', 'a'>; //=> never

let exclusiveSet2Tuple2: UnionSet2RandomList<abc>; //=> ["a", "b", "c"]
/**/
