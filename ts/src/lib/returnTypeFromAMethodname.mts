import { Newable } from './index.mjs';
import { TypeFromAFieldname } from './typeFromAFieldname.mjs';
import { ReturnTypeFromAnyFunctionType } from "./returnTypeFromAnyFunctionType.mjs";


export type ReturnTypeFromAMethodname<T extends Newable | Object, N extends string> =
    ReturnTypeFromAnyFunctionType<TypeFromAFieldname<T, N>>
;


/*
class C1 {

    method() {
        return 2;
    }

    static staticMethod() {
        return '';
    }

}


let mrt1: ReturnTypeFromAMethodname<C1, 'method'>; //=> number

let mrt2: ReturnTypeFromAMethodname<typeof C1, 'staticMethod'>; //=> string
/**/
