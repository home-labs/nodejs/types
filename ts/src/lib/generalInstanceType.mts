import { Newable } from "./newable.mjs";


export type GeneralInstanceType<T extends Newable | object> =
    T extends Newable
        ? InstanceType<T>
        : T
;

/*
class C1 { }

let gi1: GeneralInstanceType<typeof C1>; //=> C1
let gi11: GeneralInstanceType<C1>; //=> C1
/**/
