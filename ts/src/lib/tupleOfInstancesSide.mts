import { Newable } from './newable.mjs';


export type TupleOfInstancesSide<TT extends (Newable | Object)[]> = {

    [P in keyof TT]:
        TT[P] extends Newable
            ? InstanceType<TT[P]>
            : TT[P]
};


/*
class C1 {

    private p1 = 1;

}
class C2 {

    private p2 = 2;

}
class C3 {

    private p3 = 3;

}

let aot1: TupleOfInstancesSide<[typeof C1, new () => C2, C3]>; //=> [C1, C2, C3]
/**/
